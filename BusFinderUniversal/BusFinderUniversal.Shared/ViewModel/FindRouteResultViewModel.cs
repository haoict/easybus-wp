﻿using BusFinderUniversal.Model;
using BusFinderUniversal.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Linq;

namespace BusFinderUniversal.ViewModel
{
	public class FindRouteResultViewModel : ViewModelBase
	{
		ListBusViewModel currentListInstance = ServiceLocator.Current.GetInstance<ListBusViewModel>();
		private FindRouteResultModel _result;
		private string _from;
		private string _to;

		public string From
		{
			get
			{
				return _from;
			}
			set
			{
				Set("From", ref _from, value);
			}
		}
		public string To
		{
			get
			{
				return _to;
			}
			set
			{
				Set("To", ref _to, value);
			}
		}
		public FindRouteResultModel Result
		{
			get
			{
				return _result;
			}
			set
			{
				Set("Result", ref _result, value);
			}
		}
		


		public RelayCommand<ItemClickEventArgs> ResultSelectedCommand { get; private set; }

		public FindRouteResultViewModel()
		{
			Messenger.Default.Register<FindRouteResultModel>(this, ProcessFindResut);
			ResultSelectedCommand = new RelayCommand<ItemClickEventArgs>((selectedItem) => ResultSelectedHandler(selectedItem));
		}

		private void ResultSelectedHandler(ItemClickEventArgs selectedItem)
		{
			FindRouteResultDetailModel it = selectedItem.ClickedItem as FindRouteResultDetailModel;
			if (!((Frame)Window.Current.Content).Navigate(typeof(FindRouteResultMapView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
			Messenger.Default.Send(it);
			//MessageDialogHelper.Show(it.Detail);
		}

		private async void ProcessFindResut(FindRouteResultModel answer)
		{
			Result = answer;
			From = Result.From;
			To = Result.To;

			int id = 0;
			foreach (List<BusNode> ans in Result.foundAnswerList)
			{
				double distanceByBus = 0.0;
				double distanceByWalk = 0.0;
				double totalTime = 0.0;

				// tinh khoang cach di xe bus, bang cach noi moi diem dung roi tinh kc giua chung
				List<Geopoint> line = new List<Geopoint>();
				for (int i = 0; i < ans.Count - 1; i++)
				{
					line.Add(MyUtil.textToGeoList(ans[i].busStop.location).First());
					BusNode nxtNode = ans[i].nextNode;
					if (nxtNode == null)
					{
						continue;
					}

					if (nxtNode.busStop.code == ans.Last().busStop.code)
					{
						break;
					}
					while (nxtNode.busStop.code != ans[i + 1].busStop.code)
					{
						line.Add(MyUtil.textToGeoList(nxtNode.busStop.location).First());
						nxtNode = nxtNode.nextNode;
						if (nxtNode == null)
						{
							continue;
						}
					}
				}
				line.Add(MyUtil.textToGeoList(ans.Last().busStop.location).First());
				for (int i = 0; i < line.Count-1; i++)
				{
					try
					{
						distanceByBus += MyUtil.DistanceInKiloMetres(line[i], line[i+1]);
					}
					catch (Exception exc)
					{
						MessageDialogHelper.Show(exc.Message);
					}
				}

				try
				{
					distanceByWalk = MyUtil.DistanceInKiloMetres(MyUtil.textToGeoList(ans.FirstOrDefault().busStop.location).FirstOrDefault(), answer.FromPoint) + MyUtil.DistanceInKiloMetres(MyUtil.textToGeoList(ans.Last().busStop.location).FirstOrDefault(), answer.ToPoint);
					if (distanceByWalk > 1) distanceByWalk = 0.5;
				}
				catch (Exception exc)
				{
					distanceByWalk = 0.5;
				}
				
				int soLanChuyenTuyen = 0;

				string busItemsThrough = "Đi tuyến ";
				foreach (BusNode bn in ans)
				{
					if (!busItemsThrough.Contains(bn.busCode))
					{
						busItemsThrough += bn.busCode;
						busItemsThrough += ", ";

						soLanChuyenTuyen++;
					}
				}
				busItemsThrough = busItemsThrough.Remove(busItemsThrough.Length - 2);

				// thoi gian tinh bang quang duong di bus/30km/h + quang duong di bo/5km/h + so lan chuyen xe *7 phut
				totalTime = distanceByBus / 25 * 60 + distanceByWalk / 5 * 60 + soLanChuyenTuyen*10;

				FindRouteResultDetailModel tmp = new FindRouteResultDetailModel();
				tmp.ResultID = id;
				tmp.ResultName = busItemsThrough;
				tmp.ResultDescription = "Đi xe bus: " + String.Format("{0:0.00}", distanceByBus) +" km, đi bộ: " + String.Format("{0:0.00}", distanceByWalk) + " km";
				tmp.ResultTimeConsume = "Ước lượng thời gian: " + String.Format("{0:0}", totalTime) + " phút";
				tmp.Detail += AStarResultToString(ans);
				tmp.ResultNodes = answer.foundAnswerList[id];
				tmp.FromPoint = answer.FromPoint;
				tmp.ToPoint = answer.ToPoint;
				tmp.DistanceByBus = distanceByBus;
				tmp.DistanceByWalk = distanceByWalk;
				tmp.TotalTime = totalTime;
				tmp.SoLanChuyenTuyen = soLanChuyenTuyen;
				Result.ResultDetail.Add(tmp);
				id++;
			}

			// Sap xep theo so lan chuyen tuyen va quang duong di
			//Result.ResultDetail.Sort((x, y) => x.SoLanChuyenTuyen.CompareTo(y.SoLanChuyenTuyen));
			Result.ResultDetail.Sort(
				delegate(FindRouteResultDetailModel p1, FindRouteResultDetailModel p2)
				{
					int compareDate = p1.SoLanChuyenTuyen.CompareTo(p2.SoLanChuyenTuyen);
					if (compareDate == 0)
					{
						return p1.DistanceByBus.CompareTo(p2.DistanceByBus);
					}
					return compareDate;
				}
			);

			// nếu số kết quả lớn hơn 3 thì không hiện các phương án chuyển đến 3 lần bus
			if (Result.ResultDetail.Count > 3)
			{
				for (int i = 0; i < Result.ResultDetail.Count; i++)
				{
					if (Result.ResultDetail[i].SoLanChuyenTuyen >= 3)
					{
						Result.ResultDetail.RemoveAt(i);
						i--;
					}
				}
			}

			for (int i = 0; i < Result.ResultDetail.Count-1; i++)
			{
				if (Result.ResultDetail[i].ResultName == Result.ResultDetail[i+1].ResultName)
				{
					if (Result.ResultDetail[i].DistanceByWalk > Result.ResultDetail[i+1].DistanceByWalk)
					{
						Result.ResultDetail.RemoveAt(i);
					}
					else
					{
						Result.ResultDetail.RemoveAt(i+1);
					}
					i--;
				}
			}

			if (Result.FromPoint != null)
				From = await MyUtil.GetPlaceNameByLocation(Result.FromPoint);
			if (Result.ToPoint != null)
				To = await MyUtil.GetPlaceNameByLocation(Result.ToPoint);
		}

		private List<ResultObject> convertToResultObject(List<BusNode> arrNodes)
		{
			List<ResultObject> result = new List<ResultObject>();
			BusNode startNode;
			startNode = arrNodes[0];
			int foundSize = arrNodes.Count;
			for (int k = 1; k < foundSize; k++)
			{
				BusNode current = arrNodes[k];
				if (current.busCode == startNode.busCode && k < foundSize - 1)
				{
					continue;
				}
				else if (current.busCode == startNode.busCode && k == foundSize - 1)
				{
					ResultObject ro = new ResultObject(startNode.busStop, arrNodes[k].busStop, startNode.busCode);
					result.Add(ro);
				}

				else
				{
					ResultObject ro = new ResultObject(startNode.busStop, arrNodes[k].busStop, startNode.busCode);
					result.Add(ro);
					startNode = current;
				}
			}
			return result;
		}
		private string AStarResultToString(List<BusNode> foundAnswer)
		{
			if (foundAnswer == null)
			{
				return "";
			}

			List<ResultObject> searchResultElement = new List<ResultObject>();
			searchResultElement = convertToResultObject(foundAnswer);
			StringBuilder detail = new StringBuilder();
			String de = "";
			for (int index = 0; index < searchResultElement.Count; index++)
			{
				ResultObject ro = searchResultElement[index];
				if (index == 0)
				{
					detail.Append("- Đi bộ đến " + ro.getOrigin().address_name + ", bắt xe Bus " + ro.getLine()
						+ "(" + currentListInstance.FindBusItemByCode(ro.getLine()).name + ") tới "
						+ ro.getDestination().address_name + "\n");
				}
				else
				{
					detail.Append("- Từ " + ro.getOrigin().address_name + ", bắt xe Bus " + ro.getLine()
						+ "(" + currentListInstance.FindBusItemByCode(ro.getLine()).name + ") tới "
						+ ro.getDestination().address_name + "\n");
				}
				de = detail.ToString();
			}

			ResultSearchObject rso = new ResultSearchObject(searchResultElement[0].getOrigin() + " - "
					+ searchResultElement[searchResultElement.Count - 1].getDestination(), detail.ToString());

			searchResultElement.Clear();
			return de;
		}
	}
}
