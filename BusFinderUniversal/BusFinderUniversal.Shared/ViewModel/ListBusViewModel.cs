﻿using BusFinderUniversal.Model;
using BusFinderUniversal.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.Devices.Geolocation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight.Command;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;


namespace BusFinderUniversal.ViewModel
{
	public class ListBusViewModel : ViewModelBase
	{
		private BusGroup _buses;
		private ObservableCollection<BusLine> _searchBusResult;
		private BusLine _busItemSelectedFromListview;
		private string _name = "";
		private string _searchBusStr = "";
		private int _progressBarOpacity;
		private double _progressBarValue;
		public Task DataInitialization { get; private set; }
		public RelayCommand FindRouteCommand { get; private set; }
		public RelayCommand<ItemClickEventArgs> BusLineSelectedCommand { get; private set; }
		public RelayCommand ListNearbyBusCommand { get; private set; }
		public RelayCommand ListAllBusCommand { get; private set; }

		public BusLine BusItemSelectedFromListviewBuses
		{
			get
			{
				return _busItemSelectedFromListview;
			}
			set
			{
				Set("BusItemSelectedFromListviewBuses", ref _busItemSelectedFromListview, value);
			}
		}
		public ObservableCollection<BusLine> SearchBusResult
		{
			get
			{
				return _searchBusResult;
			}
			set
			{
				Set("SearchBusResult", ref _searchBusResult, value);
			}
		}
		public BusGroup Buses
		{
			get
			{
				return _buses;
			}
			set
			{
				Set("Buses", ref _buses, value);
			}
		}
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				Set("Name", ref _name, value);
			}
		}
		public string SearchBusStr
		{
			get
			{
				return _searchBusStr;
			}
			set
			{
				Set("SearchBusStr", ref _searchBusStr, value);
			}
		}
		public int ProgressBarOpacity
		{
			get
			{
				return _progressBarOpacity;
			}
			set
			{
				Set("ProgressBarOpacity", ref _progressBarOpacity, value);
			}
		}
		public double ProgressBarValue
		{
			get
			{
				return _progressBarValue;
			}
			set
			{
				Set("ProgressBarValue", ref _progressBarValue, value);
			}
		}

		private Windows.UI.ViewManagement.StatusBar statusBar;


		public ListBusViewModel()
		{
			Messenger.Default.Register<BusGroup>(this, GetProvinceInVMMessage);
		}


		private async void GetProvinceInVMMessage(BusGroup msg)
		{
			Name = msg.Name;
			Buses = msg;
			ProgressBarOpacity = 100;
			ProgressBarValue = 0;
			SearchBusResult = new ObservableCollection<BusLine>(Buses.Items);
			ProgressBarOpacity = 0;
			PropertyChanged += ListBusViewModel_PropertyChanged;
			BusLineSelectedCommand = new RelayCommand<ItemClickEventArgs>((selectedItem) => BusLineSelectedHandler(selectedItem));
			FindRouteCommand = new RelayCommand(FindRouteCommandHandler);
			ListNearbyBusCommand = new RelayCommand(ListNearbyBusCommandHandler);
			ListAllBusCommand = new RelayCommand(ListAllBusCommandHandler);
		}

		private void BusLineSelectedHandler(ItemClickEventArgs msg)
		{
			//BusItemSelectedFromListviewBuses = msg.ClickedItem as BusItem;
			if (!((Frame)Window.Current.Content).Navigate(typeof(BusLineView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
			Messenger.Default.Send(msg.ClickedItem as BusLine);
		}

		private void ViewMapCommandHandler()
		{
			if (!((Frame)Window.Current.Content).Navigate(typeof(BusLineMapView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
		}

		private async void ListNearbyBusCommandHandler()
		{
			statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
			statusBar.BackgroundColor = (App.Current.Resources["PhoneAccentBrush"] as SolidColorBrush).Color;
			statusBar.BackgroundOpacity = 0;
			statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strGettingLocation"); ;
			statusBar.ProgressIndicator.ProgressValue = null;
			await statusBar.ProgressIndicator.ShowAsync();

			Geopoint currentLocation = await MyUtil.GetCurrentLocation();
			if (currentLocation != null)
			{
				List<BusLine> nearbyList = FindNearbyBusItem(currentLocation, MyConstants.RADIUS);
				if (nearbyList == null) SearchBusResult = null;
				else SearchBusResult = new ObservableCollection<BusLine>(nearbyList);
			}
			else
			{
				SearchBusResult = null;
			}

			statusBar.ProgressIndicator.Text = "";
			statusBar.ProgressIndicator.ProgressValue = 0;
		}

		private void ListAllBusCommandHandler()
		{
			SearchBusResult = new ObservableCollection<BusLine>(Buses.Items);
		}

		private void FindRouteCommandHandler()
		{
			if (!((Frame)Window.Current.Content).Navigate(typeof(FindRouteView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
			Messenger.Default.Send(Buses);
		}
		
		private void ListBusViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "SearchBusStr")
			{
				if (SearchBusStr != "")
				{
					SearchBusResult.Clear();
					string search = MyUtil.ConvertVN(SearchBusStr.ToLower().Replace(" ", ""));

					foreach (BusLine bus in Buses.Items)
					{
						string id = MyUtil.ConvertVN(bus.code.ToLower().Replace(" ", ""));
						string name = MyUtil.ConvertVN(bus.name.ToLower().Replace(" ", "").Replace("-", ""));
						if (id.Contains(search) || name.Contains(search))
						{
							SearchBusResult.Add(bus);
						}
					}
					foreach (BusLine bus in Buses.Items)
					{
						if (SearchBusResult.Contains(bus)) continue;
						string goRoute = MyUtil.ConvertVN(bus.goRoutePath.ToLower().Replace(" ", "").Replace("-", ""));
						string reRoute = MyUtil.ConvertVN(bus.returnRoutePath.ToLower().Replace(" ", "").Replace("-", ""));

						if (goRoute.Contains(search) || reRoute.Contains(search))
						{
							SearchBusResult.Add(bus);
						}
					}
				}
				else
				{
					SearchBusResult = new ObservableCollection<BusLine>(Buses.Items);
				}
			}
		}

		/*
		private async Task GetHanoiDataAsync()
		{
			if (Buses != null)
				return;

			

			ObservableCollection<BusItem> obi = new ObservableCollection<BusItem>();
			ObservableCollection<BusStop> obs = new ObservableCollection<BusStop>();

			for (int i = 1; i <= MyConstants.NUMBER_OF_BUS; i++)
			{
				ProgressBarValue += (double)100/MyConstants.NUMBER_OF_BUS;
				string goRoutePathGeo = "";
				string RouteGoStations = "";
				string returnRoutePathGeo = "";
				List<BusStop> RouteReturnStations = new List<BusStop>();
				List<BusNode> goNode = new List<BusNode>();
				List<BusNode> returnNode = new List<BusNode>();

				Uri dataUri = new Uri("ms-appx:///DataModel/Hanoi Buses/" + i.ToString() + ".txt");
				StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
				string jsonText = await FileIO.ReadTextAsync(file);
				JsonObject jsonObject = JsonObject.Parse(jsonText)["dt"].GetObject();

				// Get Go
				JsonObject goObject = jsonObject["Go"].GetObject();
				JsonArray goGeoArray = goObject["Geo"].GetArray();
				foreach (JsonValue value in goGeoArray)
				{
					JsonObject goGeoObject = value.GetObject();

					double lng = goGeoObject["Lng"].GetNumber();
					double lat = goGeoObject["Lat"].GetNumber();
					goRoutePathGeo += lng.ToString() + "," + lat.ToString() + " ";
				}

				JsonArray goStationArray = goObject["Station"].GetArray();
				foreach (JsonValue value in goStationArray)
				{
					JsonObject goStationObject = value.GetObject();

					double lng = goStationObject["Geo"].GetObject()["Lng"].GetNumber();
					double lat = goStationObject["Geo"].GetObject()["Lat"].GetNumber();

					string code = "";
					try
					{
						code = goStationObject["ObjectID"].GetNumber().ToString();
						//code = goStationObject["code"].GetString();
					}
					catch (Exception exc)
					{
						string errMsg = exc.Message;
					}

					BusStop bs = new BusStop(code,
												goStationObject["address_name"].GetString(),
												goStationObject["busPassBy"].GetString(),
												new Geopoint(new BasicGeoposition { Latitude = lat, Longitude = lng }));
					RouteGoStations += code + " ";

					BusNode bn = new BusNode(jsonObject["code"].GetString(), null, bs, null);
					goNode.Add(bn);

					if (!obs.Any(a => a.code == code))
					{
						bs.arrayNode.Add(bn);
						obs.Add(bs);
					}
					else
					{
						obs.Where(x => x.code == code).FirstOrDefault().arrayNode.Add(bn);
					}

				}



				// Get Return
				JsonObject reObject = jsonObject["Re"].GetObject();
				JsonArray reGeoArray = reObject["Geo"].GetArray();
				foreach (JsonValue value in reGeoArray)
				{
					JsonObject reGeoObject = value.GetObject();

					double lng = reGeoObject["Lng"].GetNumber();
					double lat = reGeoObject["Lat"].GetNumber();
					returnRoutePathGeo += lng.ToString() + "," + lat.ToString() + " ";
					//returnRoutePathGeo.Add(new Geopoint(new BasicGeoposition { Latitude = lat, Longitude = lng }));
				}

				JsonArray reStationArray = reObject["Station"].GetArray();
				foreach (JsonValue value in reStationArray)
				{
					JsonObject reStationObject = value.GetObject();

					double lng = reStationObject["Geo"].GetObject()["Lng"].GetNumber();
					double lat = reStationObject["Geo"].GetObject()["Lat"].GetNumber();

					string code = "";
					try
					{
						code = reStationObject["ObjectID"].GetNumber().ToString();
					}
					catch (Exception exc)
					{
						string errMsg = exc.Message;
					}

					BusStop bs = new BusStop(code,
												reStationObject["address_name"].GetString(),
												reStationObject["busPassBy"].GetString(),
												new Geopoint(new BasicGeoposition { Latitude = lat, Longitude = lng }));
					RouteReturnStations.Add(bs);

					BusNode bn = new BusNode(jsonObject["code"].GetString(), null, bs, null);
					returnNode.Add(bn);

					if (!obs.Any(a => a.code == code))
					{
						bs.arrayNode.Add(bn);
						obs.Add(bs);
					}
					else
					{
						obs.Where(x => x.code == code).First().arrayNode.Add(bn);
					}
				}

				BusItem bus = new BusItem(jsonObject["_id"].GetNumber().ToString(),
											jsonObject["code"].GetString(),
											jsonObject["address_name"].GetString(),
											jsonObject["operationsTime"].GetString(),
											jsonObject["spacingTime"].GetString(),
											jsonObject["cost"].GetString(),
											goObject["Route"].GetString(),
											goRoutePathGeo,
											RouteGoStations,
											reObject["Route"].GetString(),
											returnRoutePathGeo,
											RouteReturnStations);
				bus.goNode = goNode;
				bus.returnNode = returnNode;
				obi.Add(bus);
			}

			Buses = new BusGroup(address_name, obi, obs);

			foreach (BusItem bi in Buses.Items)
			{
				for (int i = 0; i < bi.goNode.Count; i++)
				{
					bi.goNode[i].busItem = bi;
					if (i < bi.goNode.Count - 1)
					{
						bi.goNode[i].nextNode = bi.goNode[i + 1];
					}
						
				}
				for (int i = 0; i < bi.returnNode.Count; i++)
				{
					bi.returnNode[i].busItem = bi;
					if (i < bi.returnNode.Count - 1)
					{
						bi.returnNode[i].nextNode = bi.returnNode[i + 1];
					}
				}
			}

		}
		*/

		public BusLine FindBusItemByCode(string busCode)
		{
			var matches = Buses.Items.Where((bus) => bus.code.Equals(busCode));
			if (matches.Count() >= 1) return matches.First();
			return null;
		}

		public List<BusStop> FindNearbyBusStop(Geopoint busGeoPoint, double radius)
		{
			if (Buses == null) return null;
			if (busGeoPoint == null) return null;
			List<BusStop> listBusNearby = new List<BusStop>();
			foreach (BusStop bs in this.Buses.Stops)
			{
				if (MyUtil.DistanceInKiloMetres(MyUtil.textToGeoList(bs.location).First(), busGeoPoint) < radius)
				{
					listBusNearby.Add(bs);
				}
			}
			if (listBusNearby.Count == 0) return null;
			return listBusNearby;
		}

		public List<BusLine> FindNearbyBusItem(Geopoint location, double radius)
		{
			List<BusStop> lbs = FindNearbyBusStop(location, radius);
			if (lbs == null) return null;

			List<BusLine> lbi = new List<BusLine>();
			char[] delimiterChars = { ' ', ',', '.', ':', '\t' };

			foreach (BusStop bs in lbs)
			{
				string text = bs.busPassBy;
				string[] words = text.Split(delimiterChars);

				foreach (string s in words.Distinct())
				{
					if (s == "") { continue; }

					BusLine b = FindBusItemByCode(s);
					if (!lbi.Contains(b))
						lbi.Add(b);
				}
			}

			if (lbi.Count == 0) return null;
			return lbi;
		}

		public BusStop FindBusStopByName(string name)
		{
			var matches = Buses.Stops.Where((stop) => stop.address_name == name);
			if (matches.Count() >= 1) return matches.First();
			return null;
		}

		public List<BusStop> FindBusStopByNameUncertainly(string name)
		{
			List<BusStop> result = new List<BusStop>();
			string nameNormalized = MyUtil.ConvertVN(name.ToLower().Replace(" ", ""));
			foreach (BusStop bs in Buses.Stops)
			{
				string bsNameNormalized = MyUtil.ConvertVN(bs.address_name.ToLower().Replace(" ", ""));
				if (bsNameNormalized.Contains(nameNormalized))
				{
					result.Add(bs);
					
// 					List<BusStop> nearbyStops = this.FindNearbyBusStop(MyUtil.textToGeoList(bs.location).First(), MyConstants.RADIUS);
// 					foreach (BusStop b in nearbyStops)
// 					{
// 						result.Add(b);
// 					}
				}
					
			}
			//result = result.Distinct();
			if (result.Any())
				return result;
			return null;
		}

		internal BusStop FindBusStopByCode(string p)
		{
			var matches = Buses.Stops.Where((bus) => bus.code.Equals(p));
			if (matches.Count() >= 1) return matches.First();
			return null;
		}
	}
}
