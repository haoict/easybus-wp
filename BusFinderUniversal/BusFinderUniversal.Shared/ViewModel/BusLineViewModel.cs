﻿using BusFinderUniversal.Common;
using BusFinderUniversal.Model;
using BusFinderUniversal.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.Email;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace BusFinderUniversal.ViewModel
{
	public class BusLineViewModel : ViewModelBase
	{
		private BusLine _busLine;
		public BusLine SelectedBusLine
		{
			get
			{
				return _busLine;
			}
			set
			{
				Set("SelectedBusLine", ref _busLine, value);
			}
		}
		public RelayCommand ViewMapCommand { get; private set; }
		public RelayCommand BusErrorReportCommand { get; private set; }

		public BusLineViewModel()
		{
			Messenger.Default.Register<BusLine>(this, GetBusItemeInVMMessage);
		}

		private void GetBusItemeInVMMessage(BusLine msg)
		{
			SelectedBusLine = msg;
			PropertyChanged += BusItem_PropertyChanged;
			ViewMapCommand = new RelayCommand(ViewMapCommandHandler);
			BusErrorReportCommand = new RelayCommand(BusErrorReportCommandHandler);
		}

		private void BusItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "SelectedBusLine")
			{
			}
		}

		private void ViewMapCommandHandler()
		{
			if (!((Frame)Window.Current.Content).Navigate(typeof(BusLineMapView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
		}

		private async void BusErrorReportCommandHandler()
		{
			//
			EmailMessage em = new EmailMessage();
			em.To.Add(new EmailRecipient("caotuandung@gmail.com"));
			em.CC.Add(new EmailRecipient("hao.ict56@gmail.com"));
			em.Subject = "[Easy Bus] Báo tuyến bus sai: " + SelectedBusLine.code;
			em.Body = "Tuyến bị sai: " + SelectedBusLine.code + " - " + SelectedBusLine.name;
			em.Body += "\r\nChi tiết: <bạn có thể điền chi tiết lỗi dưới đây (sai ở đâu, vị trí nào)>";
			await EmailManager.ShowComposeNewEmailAsync(em);
			//await Windows.System.Launcher.LaunchUriAsync(new Uri("mailto:caotuandung@gmail.com"));
		}
	}
}
