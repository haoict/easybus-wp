﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusFinderUniversal.Model
{
	[Table("BusStop")]
	public class BusStop
	{
		public BusStop(string Code, string Name, string FleetOver, string geo)
		{
			this.code = Code;
			this.address_name = Name;
			this.busPassBy = FleetOver;
			this.location = geo;
			arrayNode = new List<BusNode>();
		}

		public BusStop() 
		{
			arrayNode = new List<BusNode>();
		}

		[PrimaryKey, AutoIncrement]
		public int _id { get; set; }

		public string busPassBy { get; set; }

		public string address_name { get; set; }

		public string code { get; set; }

		public string location { get; set; }

		[Ignore]
		public List<BusNode> arrayNode { get;set; }
	}
}
