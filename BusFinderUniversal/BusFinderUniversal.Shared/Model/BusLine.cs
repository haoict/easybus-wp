﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusFinderUniversal.Model
{
	[Table("BusLine")]
	public class BusLine
	{
		public BusLine(string FleetID,
			string Code,
			string Name,
			string OperationsTime,
			string Frequency,
			string Cost,
			string RouteGo,
			string RouteGoGeo,
			string RouteGoStops,
			string RouteReturn,
			string RouteReturnGeo,
			string RouteReturnStops)
		{
			this._id = Int32.Parse(FleetID);
			this.code = Code;
			this.name = Name;
			this.operationsTime = OperationsTime;
			this.spacingTime = Frequency;
			this.cost = Cost;
			this.goRoutePath = RouteGo;
			this.goRoutePathGeo = RouteGoGeo;
			this.goRouteThroughStops = RouteGoStops;
			this.returnRoutePath = RouteReturn;
			this.returnRoutePathGeo = RouteReturnGeo;
			this.returnRouteThroughStops = RouteReturnStops;

			goNode = new List<BusNode>();
			returnNode = new List<BusNode>();
		}

		public BusLine()
		{
			this._id = 0;
			goNode = new List<BusNode>();
			returnNode = new List<BusNode>();
		}

		[PrimaryKey, AutoIncrement]
		public int _id { get; set; }

		public string name { get; set; }

		public string code { get; set; }

		public string activityType { get; set; }

		public string distance { get; set; }

		public string runningTime { get; set; }

		public string spacingTime { get; set; }

		public string cost { get; set; }

		public string busType { get; set; }

		public string operationsTime { get; set; }

		public string tripsPerDay { get; set; }

		public string goRoutePath { get; set; }

		public string returnRoutePath { get; set; }

		public string goRoutePathGeo { get; set; }

		public string returnRoutePathGeo { get; set; }

		public string goRouteThroughStops { get; set; }

		public string returnRouteThroughStops { get; set; }

		[Ignore] 
		public List<BusNode> goNode { get; set; }
		[Ignore] 
		public List<BusNode> returnNode { get; set; }
	}
}
