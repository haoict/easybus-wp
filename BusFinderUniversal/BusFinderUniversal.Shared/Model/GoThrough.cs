﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusFinderUniversal.Model
{
	[Table("GoThrough")]
	public class GoThrough
	{
		public GoThrough(string busLineCode, string busStopCode, bool direction, int gtOrder, int nextBusStop)
		{
			this.busLineCode = busLineCode;
			this.busStopCode = busStopCode;
			this.direction = direction;
			this.gtOrder = gtOrder;
			this.nextBusStop = nextBusStop;
			this.distance = -1;
		}

		public GoThrough() { }

		[PrimaryKey, AutoIncrement]
		public int _id { get; set; }

		public string busLineCode { get; set; }

		public string busStopCode { get; set; }

		public bool direction { get; set; }

		public int gtOrder { get; set; }

		public string pathToNextBusStop { get; set; }

		public int nextBusStop { get; set; }

		public double distance { get; set; }
	}
}