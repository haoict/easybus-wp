﻿using BusFinderUniversal.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.ApplicationModel;

namespace BusFinderUniversal.Helper
{
	public class DatabaseHelper
	{
		public static string dbName;
		public static bool isConnect = false;
		private static SQLiteAsyncConnection conn;

		private static async Task connectDB()
		{
			if (isConnect) return;

			bool isExist = await doesDbExist();
			if (!isExist) await copyDatabase();

			conn = new SQLiteAsyncConnection(dbName);
			isConnect = true;
		}

		public static async Task<bool> doesDbExist()
		{
			bool dbexist = true;
			try
			{
				StorageFile storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync(dbName);
			}
			catch
			{
				dbexist = false;
			}
			return dbexist;
		}

		private static async Task copyDatabase()
		{
			Uri dataUri = new Uri("ms-appx:///DataModel/" + dbName);
			StorageFile databaseFile = null;
			try
			{
				databaseFile = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
			}
			catch (Exception exc)
			{
				string msg = exc.Message;
			}

			await databaseFile.CopyAsync(ApplicationData.Current.LocalFolder, dbName, NameCollisionOption.ReplaceExisting);
		}

		public static async Task<List<BusLine>> findAllBusLine()
		{
			await connectDB();
			List<BusLine> all = await conn.QueryAsync<BusLine>("Select * FROM BusLine");
			return all;
		}

		public static async Task<List<BusStop>> findAllBusStop()
		{
			await connectDB();
			List<BusStop> all = await conn.QueryAsync<BusStop>("Select * FROM BusStop");
			return all;
		}

		public static async Task<List<GoThrough>> findAllGoThrough()
		{
			await connectDB();
			List<GoThrough> all = await conn.QueryAsync<GoThrough>("Select * FROM GoThrough");
			return all;
		}

		public static async Task<BusLine> findBusLine(string code)
		{
			await connectDB();
			List<BusLine> b = await conn.QueryAsync<BusLine>("Select * FROM BusLine WHERE code = '" + code + "' LIMIT 1");
			if (b.Count == 0) return null;
			return b[0];
		}

		public static async Task<BusStop> findBusStop(string _id)
		{
			await connectDB();
			List<BusStop> bs = await conn.QueryAsync<BusStop>("Select * FROM BusStop WHERE _id = " + _id + " LIMIT 1");
			if (bs.Count == 0) return null;
			return bs[0];
		}

		public static async Task<List<BusStop>> findNextBusStop(string _id)
		{
			await connectDB();
			List<GoThrough> gtList = await conn.QueryAsync<GoThrough>("SELECT DISTINCT nextBusStop FROM GOThrough WHERE BUSStopID = " + _id);
			if (gtList.Count == 0) return null;



			List<BusStop> nextBSList = new List<BusStop>();
			foreach (var gt in gtList)
			{
				BusStop bs = await findBusStop(gt.nextBusStop.ToString());
				if (bs != null) nextBSList.Add(bs);
			}

			if (nextBSList.Count == 0) return null;
			return nextBSList;
		}
	}
}