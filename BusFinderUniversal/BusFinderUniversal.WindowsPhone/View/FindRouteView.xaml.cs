﻿using BusFinderUniversal.Helper;
using BusFinderUniversal.Model;
using BusFinderUniversal.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

//TODO: tìm đường còn chưa xử lý hướng của xe bus


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace BusFinderUniversal.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class FindRouteView : Page
	{
		private Geopoint currentLocation;
		private Geopoint fromPoint;
		private Geopoint toPoint;
		private Windows.UI.ViewManagement.StatusBar statusBar;
		private ListBusViewModel currentListInstance = ServiceLocator.Current.GetInstance<ListBusViewModel>();
		private HomeViewModel currentHomeVM = ServiceLocator.Current.GetInstance<HomeViewModel>();
		private ObservableCollection<string> SearchSuggestion { get; set; }
		private bool isProcessing;
		private bool isInitBusNodeTask { get; set; }

		public FindRouteView()
		{
			this.InitializeComponent();
			this.NavigationCacheMode = NavigationCacheMode.Required;
		}

		private async Task CurrentLocationInitializationAsync()
		{
			if (ServiceLocator.Current.GetInstance<HomeViewModel>().Buses.Name == "TP Hồ Chí Minh")
				MyMap.Center = MyConstants.DEFAULT_LOCATION_HCM;
			else
				MyMap.Center = MyConstants.DEFAULT_LOCATION;
			MyMap.ZoomLevel = 13;
			// TODO: Move focus to input To text
			var locator = new Geolocator();
			if (MyConstants.USE_LOCATION && locator.LocationStatus != PositionStatus.Disabled)
			{
				locator.DesiredAccuracyInMeters = 500;

				statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strGettingLocation"); ;
				statusBar.ProgressIndicator.ProgressValue = null;

				try
				{
					var position = await locator.GetGeopositionAsync();
					currentLocation = position.Coordinate.Point;
					MyMap.Center = currentLocation;

					inputFROM.Text = "<vị trí hiện tại>";
					if (inputFROM.Text == "<vị trí hiện tại>")
						fromPoint = currentLocation;

					Ellipse myCircle = new Ellipse();
					myCircle.Fill = new SolidColorBrush(Colors.Blue);
					myCircle.Height = 20;
					myCircle.Width = 20;
					myCircle.Opacity = 50;

					MyMap.Children.Add(myCircle);
					MapControl.SetLocation(myCircle, new Geopoint(currentLocation.Position));
					MapControl.SetNormalizedAnchorPoint(myCircle, new Point(0.5, 0.5));
				}
				catch (Exception exc)
				{
					string msg = exc.Message;
					MessageDialogHelper.Show("Không thể tìm vị trí hiện tại");
					return;
				}
				finally
				{
					statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindRouteViewReady");
					statusBar.ProgressIndicator.ProgressValue = 0;
				}
			}
		}

		/// <summary>
		/// Invoked when this page is about to be displayed in a Frame.
		/// </summary>
		/// <param address_name="e">Event data that describes how this page was reached.
		/// This parameter is typically used to configure the page.</param>
		protected async override void OnNavigatedTo(NavigationEventArgs e)
		{
			Dim.Visibility = Visibility.Visible;
			//myProgressRing.Visibility = Visibility.Visible;

			SearchSuggestion = new ObservableCollection<string>();
			this.DataContext = SearchSuggestion;

			// Khi người dùng nhập điểm đầu, điểm đến, keyboard hiện lên, màn hình không bị scroll lên trên
			var inputPane = InputPane.GetForCurrentView();
			inputPane.Showing += this.InputPaneShowing;

			// Set the background color of the status bar, and DON'T FORGET to set the opacity!
			statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
			statusBar.BackgroundColor = (App.Current.Resources["PhoneAccentBrush"] as SolidColorBrush).Color;
			statusBar.BackgroundOpacity = 0;
			await statusBar.ProgressIndicator.ShowAsync();

			if (e.NavigationMode == NavigationMode.New)
			{
				MyMap.MapElements.Clear();
				MyMap.Children.Clear();
				inputFROM.Text = "";
				inputTO.Text = "";
				isProcessing = false;
				fromPoint = null;
				toPoint = null;
				await CurrentLocationInitializationAsync();
			}

			statusBar.ProgressIndicator.Text = "Đang khởi tạo dữ liệu...";
			statusBar.ProgressIndicator.ProgressValue = null;
			
			isProcessing = false;
			try
			{
				await InitializeBusNode();
			}
			catch (Exception exc)
			{
				string message = exc.Message;
				MessageDialogHelper.Show("Lỗi: " + message + "\r\n" +
					"Vì đây là lần đầu mình làm ứng dụng WP nên không tránh khỏi lỗi. Bạn hãy vào mục Giới thiệu -> Phản hồi để mình biết và sửa lỗi giúp ứng dụng hoàn thiện hơn nhé.\r\nXin cảm ơn :-)");
				Frame.GoBack();
			}

			Dim.Visibility = Visibility.Collapsed;

			//myProgressRing.Visibility = Visibility.Collapsed;
			statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindRouteViewReady");
			statusBar.ProgressIndicator.ProgressValue = 0;
		}

		protected async override void OnNavigatedFrom(NavigationEventArgs e)
		{
			await statusBar.ProgressIndicator.HideAsync();
		}

		private void GetCurrentLocationFROM(object sender, RoutedEventArgs e)
		{
			centerMapLocation.Visibility = Visibility.Visible;
			CommandBar myBar = new CommandBar();
			myBar.IsOpen = true;
			AppBarButton AcceptLocationBtnFROM = new AppBarButton() { Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///resources/icons/appbar.check.dark.png") } };
			AcceptLocationBtnFROM.Label = "đồng ý";
			AcceptLocationBtnFROM.Click += AcceptLocationBtnFROM_Click;
			AcceptLocationBtnFROM.IsEnabled = true;
			myBar.PrimaryCommands.Add(AcceptLocationBtnFROM);
			AppBarButton CancelLocationBtn = new AppBarButton() { Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///resources/icons/cancel.dark.png") } };
			CancelLocationBtn.Label = "hủy";
			CancelLocationBtn.Click += CancelLocationSelect_Click;
			CancelLocationBtn.IsEnabled = true;
			myBar.PrimaryCommands.Add(CancelLocationBtn);
			BottomAppBar = myBar;
		}

		private void GetCurrentLocationTO(object sender, RoutedEventArgs e)
		{
			centerMapLocation.Visibility = Visibility.Visible;
			CommandBar myBar = new CommandBar();
			myBar.IsOpen = true;
			AppBarButton AcceptLocationBtnTO = new AppBarButton() { Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///resources/icons/appbar.check.dark.png") } };
			AcceptLocationBtnTO.Label = "Accept";
			AcceptLocationBtnTO.Click += AcceptLocationBtnTO_Click;
			AcceptLocationBtnTO.IsEnabled = true;
			myBar.PrimaryCommands.Add(AcceptLocationBtnTO);
			AppBarButton CancelLocationBtn = new AppBarButton() { Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///resources/icons/cancel.dark.png") } };
			CancelLocationBtn.Label = "hủy";
			CancelLocationBtn.Click += CancelLocationSelect_Click;
			CancelLocationBtn.IsEnabled = true;
			myBar.PrimaryCommands.Add(CancelLocationBtn);
			BottomAppBar = myBar;
		}

		private void CancelLocationSelect_Click(object sender, RoutedEventArgs e)
		{
			BottomAppBar = null;
			centerMapLocation.Visibility = Visibility.Collapsed;
		}

		private async void AcceptLocationBtnTO_Click(object sender, RoutedEventArgs e)
		{
			BottomAppBar = null;
			centerMapLocation.Visibility = Visibility.Collapsed;
			inputTO.Text = "(" + MyMap.Center.Position.Longitude.ToString() + ", " + MyMap.Center.Position.Latitude.ToString() + ")";
			toPoint = new Geopoint(MyMap.Center.Position);

			MyMap.Children.Clear();
			if (fromPoint != null)
				DrawImageToMap("/resources/icons/appbar.location.round.light.png", fromPoint);
			DrawImageToMap("/resources/icons/appbar.location.round.light.png", toPoint);

// 			if (inputFROM.Text != "" && inputTO.Text != "")
// 				FindRoute();

			string placeName = await MyUtil.GetPlaceNameByLocation(toPoint);
			if (placeName != "")
				inputTO.Text = placeName;
		}

		private async void AcceptLocationBtnFROM_Click(object sender, RoutedEventArgs e)
		{
			BottomAppBar = null;
			centerMapLocation.Visibility = Visibility.Collapsed;
			inputFROM.Text = "(" + MyMap.Center.Position.Longitude.ToString() + ", " + MyMap.Center.Position.Latitude.ToString() + ")";
			fromPoint = new Geopoint(MyMap.Center.Position);

			MyMap.Children.Clear();
			DrawImageToMap("/resources/icons/appbar.location.round.light.png", fromPoint);
			if (toPoint != null)
				DrawImageToMap("/resources/icons/appbar.location.round.light.png", toPoint);

			string placeName = await MyUtil.GetPlaceNameByLocation(fromPoint);
			if (placeName != "")
				inputFROM.Text = placeName;
		}

		private void DrawImageToMap(string imagePath, Geopoint location)
		{
			Image iconStart = new Image();
			BitmapImage bi = new BitmapImage(new Uri("ms-appx://" + imagePath));
			iconStart.Source = bi;
			MyMap.Children.Add(iconStart);
			MapControl.SetLocation(iconStart, location);
			MapControl.SetNormalizedAnchorPoint(iconStart, new Point(0.5, 0.5));
		}

		private void FindRoute_Click(object sender, RoutedEventArgs e)
		{
// 			string tmp = inputFROM.Text;
// 			inputFROM.Text = inputTO.Text;
// 			inputTO.Text = tmp;
			SearchSuggestion.Clear();
			if (inputFROM.Text != "" && inputTO.Text != "")
				FindRoute();
			else
				MessageDialogHelper.Show("Bạn hãy nhập điểm đầu và điểm cuối");
		}

		private void MyMap_MapTapped(Windows.UI.Xaml.Controls.Maps.MapControl sender, Windows.UI.Xaml.Controls.Maps.MapInputEventArgs args)
		{
			this.Focus(FocusState.Pointer);
		}

		private void FROMBox_KeyDown(object sender, KeyRoutedEventArgs e)
		{
			if (e.Key.Equals(Windows.System.VirtualKey.Enter))
			{
				if (inputTO.Text != "")
				{
					this.Focus(FocusState.Pointer);
					return;
				}
				FocusManager.TryMoveFocus(FocusNavigationDirection.Next);
			}
		}

		private void TOBox_KeyDown(object sender, KeyRoutedEventArgs e)
		{
			if (e.Key.Equals(Windows.System.VirtualKey.Enter))
			{
				if (inputFROM.Text == "" || inputTO.Text == "")
				{
					return;
				}
				FocusManager.TryMoveFocus(FocusNavigationDirection.Next);
			}
		}

		private async Task<Geopoint> GetCurrentLocation()
		{
			var locator = new Geolocator();
			if (!MyConstants.USE_LOCATION || locator.LocationStatus == PositionStatus.Disabled)
			{
				MessageDialogHelper.Show("Dịch vụ tìn vị trí bị tắt", "");
				return null;
			}
			locator.DesiredAccuracyInMeters = (uint)(MyConstants.RADIUS * 1000);

			try
			{
				var position = await locator.GetGeopositionAsync();
				return position.Coordinate.Point;
			}
			catch (Exception exc)
			{
				string msg = exc.Message;
				MessageDialogHelper.Show("Không thể tìm vị trí hiện tại");
				return null;
			}
		}

		private async Task<Geopoint> GetPositionByPlaceName(string name)
		{
			try
			{
				Geopoint queryHintPoint = currentLocation;
				var result = await MapLocationFinder.FindLocationsAsync(name, queryHintPoint);
				if (result.Status == MapLocationFinderStatus.Success)
				{
					BasicGeoposition location = new BasicGeoposition();
					location.Latitude = result.Locations[0].Point.Position.Latitude;
					location.Longitude = result.Locations[0].Point.Position.Longitude;
					return new Geopoint(location);
				}
			}
			catch (Exception exc)
			{
				string message = exc.Message;
			}
			return null;
		}

		private void FROMBox_LostFocus(object sender, RoutedEventArgs e)
		{
			SearchSuggestion.Clear();
		}

		private void FROMBox_GotFocus(object sender, RoutedEventArgs e)
		{
		}

		private void TOBox_LostFocus(object sender, RoutedEventArgs e)
		{
			//if (SearchSuggestion.Count != 0)
			//{
// 				SearchSuggestion.Clear();
// 				if (inputFROM.Text != "" && inputTO.Text != "")
// 					FindRoute();
			//}
			
			
		}

		private void TOBox_GotFocus(object sender, RoutedEventArgs e)
		{
		}

		// Khi mở keyboard lên, màn hình không bị scroll lên trên
		private void InputPaneShowing(InputPane sender, InputPaneVisibilityEventArgs args)
		 {
			args.EnsuredFocusedElementInView = true;
			inputField.Margin = new Thickness(0, 0, 0, args.OccludedRect.Height);
		 }

		private async void FindRoute()
		{
			if (isProcessing)
			{
				return;
			}
			isProcessing = true;

			//await currentHomeVM.initializeBusNodeTask;

			statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindingRoute"); ;
			statusBar.ProgressIndicator.ProgressValue = null;

			// Prepare list of BusStop as result from user input
			List<BusStop> fromBusStopList;
			List<BusStop> toBusStopList;
			List<List<BusNode>> foundAnswerList = new List<List<BusNode>>();

			try
			{
				if (fromPoint != null)
				{
					fromBusStopList = currentListInstance.FindNearbyBusStop(fromPoint, MyConstants.RADIUS);
				}
				else if (inputFROM.Text == "<vị trí hiện tại>")
				{
					if (currentLocation == null)
						currentLocation = await GetCurrentLocation();
					if (currentLocation == null)
					{
						inputFROM.Text = "";
						return;
					}

					fromPoint = currentLocation;
					fromBusStopList = currentListInstance.FindNearbyBusStop(fromPoint, MyConstants.RADIUS);
				}
				else
				{
					fromBusStopList = currentListInstance.FindBusStopByNameUncertainly(inputFROM.Text);
					fromPoint = await GetPositionByPlaceName(inputFROM.Text);
				}
				if (fromBusStopList == null && inputFROM.Text != "<vị trí hiện tại>")
				{
					Geopoint tmpPoint = await GetPositionByPlaceName(inputFROM.Text);
					fromBusStopList = currentListInstance.FindNearbyBusStop(tmpPoint, MyConstants.RADIUS);
					fromPoint = tmpPoint;
				}
				if (fromBusStopList == null)
				{
					isProcessing = false;
					MessageDialogHelper.Show("Không tìm thấy xe bus nào gần điểm bắt đầu");
					statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindRouteViewReady"); ;
					statusBar.ProgressIndicator.ProgressValue = 0;
					return;
				}

				if (toPoint != null)
				{
					toBusStopList = currentListInstance.FindNearbyBusStop(toPoint, MyConstants.RADIUS);
				}
				else if (inputTO.Text == "<vị trí hiện tại>")
				{
					toPoint = currentLocation;
					toBusStopList = currentListInstance.FindNearbyBusStop(toPoint, MyConstants.RADIUS);
				}
				else
				{
					toBusStopList = currentListInstance.FindBusStopByNameUncertainly(inputTO.Text);
					toPoint = await GetPositionByPlaceName(inputTO.Text);
				}
				if (toBusStopList == null && inputTO.Text == "<vị trí hiện tại>")
				{
					Geopoint tmpPoint = await GetPositionByPlaceName(inputTO.Text);
					toBusStopList = currentListInstance.FindNearbyBusStop(tmpPoint, MyConstants.RADIUS);
					toPoint = tmpPoint;
				}
				if (toBusStopList == null)
				{
					isProcessing = false;
					MessageDialogHelper.Show("Không tìm thấy xe bus nào gần điểm kết thúc");
					statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindRouteViewReady"); ;
					statusBar.ProgressIndicator.ProgressValue = 0;
					return;
				}

				// Let's do it
				foreach (BusStop f in fromBusStopList)
				{
					foreach (BusStop t in toBusStopList)
					{
						if (f.code == t.code) continue;
						// Begin Algorithm
						AStarAlgo AS1 = new AStarAlgo(f, t);
						AS1.algorithm();
						if (AS1.answer.Count() != 0)
						{
							// We found an answer
							foundAnswerList.Add(AS1.answer);
						}
					}
				}
			}
			catch (Exception exc)
			{
				string message = exc.Message;
				MessageDialogHelper.Show("Lỗi: " + message + "\r\n" +
					"Vì đây là lần đầu mình làm ứng dụng WP nên không tránh khỏi lỗi. Bạn hãy vào mục Giới thiệu -> Phản hồi để mình biết và sửa lỗi giúp ứng dụng hoàn thiện hơn nhé.\r\nXin cảm ơn :-)");
			}

			statusBar.ProgressIndicator.Text = MyUtil.GetStringResource("strFindRouteViewReady"); ;
			statusBar.ProgressIndicator.ProgressValue = 0;

			if (foundAnswerList.Count == 0)
			{
				MessageDialogHelper.Show("Không tìm thấy kết quả nào");
				isProcessing = false;
				//fromPoint = null;
				//toPoint = null;
				return;
			}


			FindRouteResultModel result = new FindRouteResultModel();
			result.From = inputFROM.Text;
			result.To = inputTO.Text;
			result.FromPoint = fromPoint;
			result.ToPoint = toPoint;
			result.foundAnswerList = foundAnswerList;
			if (!((Frame)Window.Current.Content).Navigate(typeof(FindRouteResultView)))
			{
				throw new Exception("NavigationFailedExceptionMessage");
			}
			Messenger.Default.Send(result);

			//fromPoint = null;
			//toPoint = null;
			//inputFROM.Text = "";
			//inputTO.Text = "";
			isProcessing = false;
			//string detail = "";
			//foreach (List<BusNode> ans in foundAnswerList)
			//{
			//	// print to map
			//	/*
			//	for (int i = 0; i < foundAnswer.Count(); i++)
			//	{
			//		findShortestRoute(SampleDataSource.FindBusStopByCode(foundAnswer[i].stopCode).location,
			//			SampleDataSource.FindBusStopByCode(foundAnswer[i + 1].stopCode).location);
			//	}
			//	*/
			//	// print to string

			//	detail += AStarResultToString(ans);
			//	detail += "*********************\r\n";

			//}
			//MessageDialogHelper.Show(detail);

			

		}
		//
		private List<ResultObject> convertToResultObject(List<BusNode> arrNodes)
		{
			List<ResultObject> result = new List<ResultObject>();
			BusNode startNode;
			startNode = arrNodes[0];
			int foundSize = arrNodes.Count();
			for (int k = 1; k < foundSize; k++)
			{
				BusNode current = arrNodes[k];
				if (current.busCode == startNode.busCode && k < foundSize - 1)
				{
					continue;
				}
				else if (current.busCode == startNode.busCode && k == foundSize - 1)
				{
					ResultObject ro = new ResultObject(startNode.busStop, arrNodes[k].busStop, startNode.busCode);
					result.Add(ro);
				}

				else
				{
					ResultObject ro = new ResultObject(startNode.busStop, arrNodes[k].busStop, startNode.busCode);
					result.Add(ro);
					startNode = current;
				}
			}
			return result;
		}
		private string AStarResultToString(List<BusNode> foundAnswer)
		{
			if (foundAnswer == null)
			{
				return "";
			}

			List<ResultObject> searchResultElement = new List<ResultObject>();
			searchResultElement = convertToResultObject(foundAnswer);
			StringBuilder detail = new StringBuilder();
			String de = "";
			for (int index = 0; index < searchResultElement.Count(); index++)
			{
				ResultObject ro = searchResultElement[index];
				if (index == 0)
				{
					detail.Append("- Di chuyển đến " + ro.getOrigin().address_name + ", bắt xe Bus " + ro.getLine()
						+ "(" + currentListInstance.FindBusItemByCode(ro.getLine()).name + ") tới "
						+ ro.getDestination().address_name + "\n");
				}
				else
				{
					detail.Append("- Từ " + ro.getOrigin().address_name + ", bắt xe Bus " + ro.getLine()
						+ "(" + currentListInstance.FindBusItemByCode(ro.getLine()).name + ") tới "
						+ ro.getDestination().address_name + "\n");
				}
				de = detail.ToString();
			}

			ResultSearchObject rso = new ResultSearchObject(searchResultElement[0].getOrigin() + " - "
					+ searchResultElement[searchResultElement.Count() - 1].getDestination(), detail.ToString());

			searchResultElement.Clear();
			return de;
		}

		//AutoSuggestBox
		private void suggestions_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
		{
			if (currentListInstance.Buses == null) return;

				if (sender.Name == "inputFROM" && sender.Text == "")
				{
					fromPoint = null;
				}
				if (sender.Name == "inputTO" && sender.Text == "")
				{
					toPoint = null;
				}

				SearchSuggestion.Clear();

				if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
				{
					if (sender.Text == "" || sender.Text == "<vị trí hiện tại>") return;

					foreach (BusStop bs in currentListInstance.Buses.Stops)
					{
						string t1 = MyUtil.VNTextNormalize(sender.Text);
						string t2 = MyUtil.VNTextNormalize(bs.address_name);
						if (t2.Contains(t1))
						{
							SearchSuggestion.Add(bs.address_name);
						}
					}
				}

			
		}

		private void suggestions_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
		{

			if (sender.Name == "inputFROM")
			{
				inputFROM.Text = args.SelectedItem as string;
			}
			else
			{
				inputTO.Text = args.SelectedItem as string;
			}

			

			//SearchSuggestion.Clear();
			FocusManager.TryMoveFocus(FocusNavigationDirection.Next);
		}

		private async void ToggleTracking(object sender, RoutedEventArgs e)
		{
			var locator = new Geolocator();
			if (!MyConstants.USE_LOCATION || locator.LocationStatus == PositionStatus.Disabled)
			{
				MessageDialogHelper.Show("Dịch vụ tìm vị trí bị tắt");
				return;
			}

			if (currentLocation == null)
			{
				currentLocation = await GetCurrentLocation();
			}
			if (currentLocation == null) return;

			await MyMap.TrySetViewAsync(currentLocation, 16D);

			Ellipse myCircle = new Ellipse();
			myCircle.Fill = new SolidColorBrush(Colors.Blue);
			myCircle.Height = 20;
			myCircle.Width = 20;
			myCircle.Opacity = 50;

			MyMap.Children.Add(myCircle);
			MapControl.SetLocation(myCircle, new Geopoint(currentLocation.Position));
			MapControl.SetNormalizedAnchorPoint(myCircle, new Point(0.5, 0.5));
		}

		public async Task InitializeBusNode()
		{
			if (currentHomeVM.isInitBusNodeTask == true) return;
			// Doan nay them may cai node de phuc vu tim duong, lang nhang qua
			List<GoThrough> _ogt = await DatabaseHelper.findAllGoThrough();

			statusBar.ProgressIndicator.ProgressValue = 0;

			for (int i = 0; i < currentHomeVM.Buses.Items.Count; i++)
			//foreach (BusLine bi in currentHomeVM.Buses.Items)
			{
				BusLine bi = currentHomeVM.Buses.Items[i];
				int percent = i * 100 / currentHomeVM.Buses.Items.Count;
				statusBar.ProgressIndicator.Text = "Đang khởi tạo dữ liệu... " + percent.ToString() + "%";
				statusBar.ProgressIndicator.ProgressValue = (double)i / currentHomeVM.Buses.Items.Count;
				IEnumerable<GoThrough> gth = _ogt.Where(s => s.busLineCode == bi.code);
				foreach (GoThrough gt__ in gth)
				{
					List<BusStop> __bs = MyUtil.textToBusStopList(gt__.busStopCode.ToString());
					BusNode bn = new BusNode(bi.code, null, __bs.First(), null);
					__bs.First().arrayNode.Add(bn);

					if (gt__.direction == false)
						bi.goNode.Add(bn);
					else
						bi.returnNode.Add(bn);
				}
			}

			foreach (BusLine bi in currentHomeVM.Buses.Items)
			{
				for (int i = 0; i < bi.goNode.Count; i++)
				{
					bi.goNode[i].busItem = bi;
					if (i < bi.goNode.Count - 1)
					{
						bi.goNode[i].nextNode = bi.goNode[i + 1];
					}
				}
				for (int i = 0; i < bi.returnNode.Count; i++)
				{
					bi.returnNode[i].busItem = bi;
					if (i < bi.returnNode.Count - 1)
					{
						bi.returnNode[i].nextNode = bi.returnNode[i + 1];
					}
				}
			}
			currentHomeVM.isInitBusNodeTask = true;
		}
	}
}

// TODO: di chuyeenr trong thoi gian bao lau + hien ra map  